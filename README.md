# Sentient Mushes: World

Explore a vast text world, with rich information on angles and distances (to locate yourself and other objects), commands to move, turn, and to interact with a very rich environment.

By the way, this is a multiplayer game, so interact with your friends in this virtual scene as well! :)