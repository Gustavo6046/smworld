const SMWorld = require('.');
const fs = require('fs');

// dream & house
let startRegion = new SMWorld.Region('the dream of the Mirror Dome', -12000, -12000, 300, 300000, true); // name, X, Y, radius/size, integrity, starting = false
let bedroom = new SMWorld.Region('the bedroom', 0, 0, 60);
let corridorE = new SMWorld.Region('east half of the corridor', 75, 100, 32);
let corridorW = new SMWorld.Region('west half of the corridor', 0, 100, 32);
let kitchen = new SMWorld.Region('the kitchen', 75, 250, 70);
let lroom = new SMWorld.Region('the living room', 75, -250, 90);
let byard = new SMWorld.Region('the backyard', 75, -500, 120);

startRegion.postInit();
bedroom.postInit();
corridorE.postInit();
corridorW.postInit();
kitchen.postInit();
lroom.postInit();

// stairwell & attic
let swu = new SMWorld.Region('the stairwell (upper)', 0, 250, 110);
let swm = new SMWorld.Region('the stairwell (middle)', -300, 250, 110);
let swd = new SMWorld.Region('the stairwell (lower)', -600, 250, 110);
let attic = new SMWorld.Region('the attic', -600, 400, 160);
let balcony = new SMWorld.Region('the balcony', -300, -700, 120);

swu.postInit();
swm.postInit();
swd.postInit();

// -------------

// starting dream -> bedroom
new SMWorld.Door('a very strange gate', startRegion.id, { to: bedroom.id, angle: Math.random() * Math.PI * 2, radius: 35 }, startRegion.location, startRegion.radius).postInit();

// corridor E <-> corridor W
new SMWorld.Door('the west half', corridorE.id, { to: corridorW.id, angle: Math.PI, radius: 8 }, corridorE.location, corridorE.radius).postInit();
new SMWorld.Door('the east half', corridorW.id, { to: corridorE.id, angle: 0, radius: 8 }, corridorW.location, corridorW.radius).postInit();

// bedroom <-> corridor W
SMWorld.Door.create('the corridor door', bedroom, corridorW, Math.PI * 3 / 2, 8).postInit();
SMWorld.Door.create('the bedroom door', corridorW, bedroom, Math.PI / 2, 8).postInit();

// kitchen <-> corridor E
SMWorld.Door.create('the kitchen door', corridorE, kitchen, Math.PI * 3 / 2, 8).postInit();
SMWorld.Door.create('the corridor door', kitchen, corridorE, Math.PI / 2, 8).postInit();

// living room <-> corridor E
SMWorld.Door.create('the living room door', corridorE, lroom, Math.PI / 2, 8).postInit();
SMWorld.Door.create('the corridor door', lroom, corridorE, Math.PI * 3 / 2, 8).postInit();

// living room <-> backyard
SMWorld.Door.create('the backyard door', lroom, byard, Math.PI / 2, 8).postInit();
SMWorld.Door.create('the door', byard, lroom, Math.PI * 3 / 2, 8).postInit();

// stairwell
SMWorld.Door.create('the stairwell passage', corridorW, swd, Math.PI * 3 / 2, 10).postInit();
SMWorld.Door.create('the corridor passage', swd, corridorW, Math.PI / 2, 10).postInit();

SMWorld.Door.create('upstairs', swd, swm, Math.PI * 3 / 2, 15).postInit();
SMWorld.Door.create('downstairs', swm, swd, Math.PI / 2, 15).postInit();

SMWorld.Door.create('upstairs', swm, swu, Math.PI * 3 / 2, 15).postInit();
SMWorld.Door.create('downstairs', swu, swm, Math.PI / 2, 15).postInit();

// balcony & attic
SMWorld.Door.create('the balcony door', swm, balcony, 0, 9).postInit();
SMWorld.Door.create('the stairwell door', balcony, swm, Math.PI, 9).postInit();

SMWorld.Door.create('the attic door', swu, attic, Math.PI * 3 / 2, 9).postInit();
SMWorld.Door.create('the stairwell door', attic, swu, Math.PI / 2, 9).postInit();

// -------------

new SMWorld.LocalEntity('the stairwell pillar', swu.location, swu.id, 50, true, 500).postInit();
new SMWorld.LocalEntity('the stairwell pillar', swm.location, swm.id, 50, true, 500).postInit();
new SMWorld.LocalEntity('the stairwell pillar', swd.location, swd.id, 50, true, 500).postInit();

// -------------


// dream
new SMWorld.Illusion('a chair (illusory)', SMWorld.AnyPolarPoint.fromCartesian(0, 25).translate(startRegion.location), startRegion.id, 13, false, 100, Math.PI * 1.5).postInit();
new SMWorld.Illusion('a table (illusory)', SMWorld.AnyPolarPoint.fromCartesian(-5, 30).translate(startRegion.location), startRegion.id, 13, false, 100, Math.PI).postInit();
new SMWorld.Illusion('another chair (illusory)', SMWorld.AnyPolarPoint.fromCartesian(5, 22).translate(startRegion.location), startRegion.id, 13, false, 100, -Math.PI / 3).postInit();
new SMWorld.Illusion('a small dragon (illusory)', SMWorld.AnyPolarPoint.fromCartesian(-15, 90).translate(startRegion.location), startRegion.id, 13, false, 100, Math.PI).postInit();
new SMWorld.Illusion('something cloudy (illusory)', SMWorld.AnyPolarPoint.fromCartesian(-20, -18).translate(startRegion.location), startRegion.id, 13, false, 100, Math.PI).postInit();
new SMWorld.Illusion('a smaller dragon (illusory)', SMWorld.AnyPolarPoint.fromCartesian(-15, 90).translate(startRegion.location), startRegion.id, 13, false, 100, Math.PI).postInit();

// bedroom
new SMWorld.AngledLocalEntity('a bed', bedroom.location.translate(new SMWorld.AnyPolarPoint(bedroom.radius - 2.4, Math.PI - 0.1)), bedroom.id, 15, true, 100, 0, Math.PI / 2, 0).postInit();
new SMWorld.AngledLocalEntity('a TV', bedroom.location.translate({ x: 16, y: -2 }), bedroom.id, 4, true, 80, Math.PI / 3, Math.PI / 2, 0).postInit();
new SMWorld.AnimalEntity('Luke (male dog)', byard.location.translateCartesian(-3, 75), byard.id, { aggressive: true, integrity: 60, sounds: { idle: ['Woof!', 'Arf!'], angry: ['Arrf!', 'Rrrgh!', 'Woof woof!', 'Grrrr!', 'Bow-wow!'] }, meleeDamage: 9, groundSpeed: 9, soundIntensity: 70 }).postInit();
new SMWorld.AnimalEntity('Dina (female cat)', bedroom.location.translateCartesian(14, -2.5), bedroom.id, { aggressive: true, integrity: 60, meleeDamage: 13, sounds: { idle: ['Meow!', 'Prrr!', 'Prr!', 'Meoow!'], angry: ['Mow!', 'Rrrgh!', 'Grrrr!', 'Puff!'] }, groundSpeed: 14, soundIntensity: 60 }).postInit();
new SMWorld.AnimalEntity('Kathy (female goose)', balcony.location.translateCartesian(40, 0), balcony.id, { integrity: 50, meleeDamage: 13, sounds: { idle: ['Caw!', 'Caaw!', 'Honk!', 'Squaw!'] }, groundSpeed: 50, soundIntensity: 94 }).postInit();

// living room
new SMWorld.AngledLocalEntity('a TV', lroom.location.translateCartesian(40, 0), lroom.id, 2, true, 80, Math.PI, Math.PI / 2, 0).postInit();

fs.writeFileSync(process.argv[2], JSON.stringify(SMWorld.getSave()));
