// eslint-disable-next-line no-undef
var SMWorld = require('smworld');
// eslint-disable-next-line no-undef
var socket = io();
var flaker = require('flaker')();
var password = null;
var escape = document.createElement('textarea');
var me = null;
var updates = 0;
var fullUpdates = 0;

var wd;
var hg;

var sounds = new Map();
var situation = null;

let buffers = [
    document.createElement('canvas')
];

buffers.get = function getBuffer(apply) {
    apply(buffers[buffers.buf]);
    return buffers[buffers.buf = ((buffers.buf + 1) % buffers.length)];
};

buffers.buf = 0;

function parseCommand(cmd) {
    let args = cmd.split(' ');
    let c = args.shift();

    let res = null;

    if (c === 'move') {
        let m = {
            forward: 0,
            rightward: 0,
            turn: 0
        };

        args.forEach((a) => {
            let param = /^(-?\d+(?:\.\d+)?)([fbrl]|[rl]t)$/.exec(a);

            if (param == null) return;

            let amount = +param[1];
            let type = param[2];

            if (isNaN(amount)) return;

            if (type.length > 1) {
                if (type[0] === 'l') amount *= -1;
                m.turn += amount;
            }

            else {
                if (type[0] === 'l' || type[0] === 'b') amount *= -1;

                if (type[0] === 'r' || type[0] === 'l')
                    m.rightward += amount;

                else
                    m.forward += amount;
            }
        });

        res = [
            {type: 'move', args: [m.forward, m.rightward]},
            {type: 'turn', args: [m.turn * Math.PI / 180]}
        ];
    }

    else if ((c === 'front' || c === 'forward') && args.length > 0) {
        res = [
            {type: 'move', args: [+args[0], 0]}
        ];
    }

    else if ((c === 'back' || c === 'backward') && args.length > 0) {
        res = [
            {type: 'move', args: [-args[0], 0]}
        ];
    }

    else if ((c === 'right' || c === 'rightward') && args.length > 0) {
        res = [
            {type: 'move', args: [0, +args[0]]}
        ];
    }

    else if ((c === 'left' || c === 'leftward') && args.length > 0) {
        res = [
            {type: 'move', args: [0, -args[0]]}
        ];
    }

    else if ((c === 'turn' || c === 'rotate') && args.length > 0) {
        res = [
            {type: 'turn', args: [+args[0] * Math.PI / 180]}
        ];
    }

    return res;
}

function cSelectMove() {
    let params = $('<div></div>');

    let action = $('#action');
    action[0].innerHTML = '';

    let sel = $('<select><option disabled selected value> -- select a command -- </option><option value="move">Move</option><option value="turn">Turn</option></select>');
    sel.on('input', () => {
        if (sel.val() === 'move')
            cSelectMove();

        else if (sel.val() === 'turn')
            cSelectTurn();
    });
    sel.appendTo(action);
    
    $('<span>Forward:</span>').appendTo(params);
    
    let fmove = $('<input type="number" min="-50" max="50" step="0.1" value="0">');
    fmove.appendTo(params);
    
    $('<span>Rightward:</span>').appendTo(params);

    let smove = $('<input type="number" min="-50" max="50" step="0.1" value="0">');
    smove.appendTo(params);

    params.appendTo(action);

    let but = $('<button type="button">Move</button>');
    but.on('click', () => {
        sendCommand({
            type: 'move',
            args: [fmove.val(), smove.val()]
        });
    });
    but.appendTo(action);
}

function cSelectTurn() {
    let action = $('#action');
    action[0].innerHTML = '';

    let sel = $('<select><option disabled selected value> -- select a command -- </option><option value="move">Move</option><option value="turn">Turn</option></select>');
    sel.on('input', () => {
        if (sel.val() === 'move')
            cSelectMove();

        else if (sel.val() === 'turn')
            cSelectTurn();
    });
    sel.appendTo(action);

    $('<div>Degrees (+: to the right, -: to the left):</div>').appendTo($(action));
    
    let dturn = $('<input type="number" min="0" max="12" step="0.1" value="0">');
    dturn.appendTo(action);

    let but = $('<button type="button">Turn</button>');
    but.on('click', () => {
        let v = dturn.val() * Math.PI / 180;

        sendCommand({
            type: 'turn',
            args: [v]
        });
    });
    but.appendTo(action);
}

function commandInput() {
    let action = $('#action');

    action[0].innerHTML = '<div>Insert command:</div>';

    let inp = $('<input type="text" placeholder="move 15f 4l 1l 22.5rt"></input>');
    let but = $('<button type="button">Send</button>');

    function send(evt) {
        if (evt.keyCode === 13) {
            let cmd = parseCommand(inp.val());

            if (cmd == null)
                inp.val('Invalid command.');

            else {
                cmd.forEach((c) => {
                    sendCommand(c);
                });
            }
        }
    }
    
    inp.on('keypress', (evt) => send(evt));
    but.on('click', (evt) => send(evt));

    inp.appendTo(action);
    but.appendTo(action);
}

// eslint-disable-next-line no-unused-vars
function commandChoice() {
    let action = $('#action');

    action[0].innerHTML = '<div>Next action:</div>';
    let sel = $('<select><option disabled selected value> -- select a command -- </option><option value="move">Move</option><option value="turn">Turn</option></select>');
    sel.on('input', () => {
        if (sel.val() === 'move')
            cSelectMove();

        else if (sel.val() === 'turn')
            cSelectTurn();
    });
    sel.appendTo(action);
}

function updateConsole(w=wd, h=hg) {
    let logs = [];
    let fontSize = 14; // px
    let fontMargin = 1; // px

    if (me == null) return;

    let cons = document.querySelector('.game-console');
    let jcons = $(cons);

    let canvas = buffers.get((canv) => {
        canv.width = w;
        canv.height = h;
        jcons.empty().replaceWith($(canv));
    });

    let ctx = canvas.getContext('2d');

    ctx.lineWidth = 2;

    ctx.translate(canvas.width / 2, canvas.height / 2);
    ctx.scale(1.5, 1.5);
    ctx.translate(-me.location.x, -me.location.y);

    let reg = me.getRegion();

    if (reg.constructor === SMWorld.RectangularRegion) {
        let box = reg.boundingBox();
        ctx.strokeRect(box.x1, box.y1, box.x2 - box.x1, box.y2 - box.y1);
    }

    else {
        ctx.beginPath();
        ctx.arc(reg.location.x, reg.location.y, reg.radius, 0, Math.PI * 2);
        ctx.stroke();
    }

    ctx.log = function(data = '') {
        logs.push(data);
    };

    ctx.log();

    ctx.createField = function() {
        let res = {};
        let cursor = logs.length;

        res.log = function(data = '') {
            logs = logs.slice(0, cursor++).concat([data].concat(logs.slice(cursor)));
        };

        res.prelog = function(data = '') {
            logs = logs.slice(0, cursor).concat([data].concat(logs.slice(cursor)));
        };

        return res;
    };

    ctx.log(`    - PERCEPTIVE STATUS FOR: ${me.name} -`);
    ctx.log();
    ctx.log(' * Situation Awareness: OK.');
    ctx.log(`You are in ${me.getRegion().name}.`);
    ctx.log(`Physical integrity: ${me.integrity}%.`);

    let sitField = ctx.createField();

    if (situation != null)
        sitField.log(situation);

    ctx.log();
    ctx.log(' * Vision: OK.');
    ctx.log('You can see...');
    
    let v = me.vision();

    ctx.lineWidth = 2;
    ctx.strokeStyle = '#99880040';

    ctx.beginPath();

    let b = me.location;
    let a = b.translate(new SMWorld.AnyPolarPoint(60, me.angle - me.fov / 2));
    let c = b.translate(new SMWorld.AnyPolarPoint(60, me.angle + me.fov / 2));

    ctx.fillStyle = '#BBAA0050';
    ctx.textAlign = 'center';
    ctx.font = '11px monospace';
    ctx.fillText(me.name, me.location.x, me.location.y);

    ctx.moveTo(a.x, a.y);
    ctx.lineTo(b.x, b.y);
    ctx.lineTo(c.x, c.y);
    ctx.stroke();
    
    v.forEach((seen, i) => {
        let last = i === v.length - 1;
        let a = Math.round(seen.angle * 180 / Math.PI);

        ctx.log(`...${seen.entity.name}, ${Math.round(seen.distance)} meters away${Math.abs(a) * 180 / Math.PI < 4 ? '' : `, at ${Math.abs(a)}° ${a > 0 ? 'rightward' : 'leftward'} ${last ? '.' : ';'}`}.`);

        ctx.beginPath();
        ctx.arc(seen.entity.location.x, seen.entity.location.y, seen.entity.radius, 0, Math.PI * 2);
        ctx.stroke();

        if (seen.entity.angle != null && seen.entity.fov != null) {
            let fovSize = 10;

            if (seen.entity.constructor === SMWorld.PlayerEntity || seen.entity.constructor === SMWorld.AnimalEntity)
                fovSize = 25;

            let en = seen.entity;

            let b = en.location;
            let a = b.translate(new SMWorld.AnyPolarPoint(fovSize, en.angle - en.fov / 2));
            let c = b.translate(new SMWorld.AnyPolarPoint(fovSize, en.angle + en.fov / 2));

            ctx.beginPath();
            ctx.moveTo(a.x, a.y);
            ctx.lineTo(b.x, b.y);
            ctx.lineTo(c.x, c.y);
            ctx.stroke();
        }

        ctx.fillStyle = '#BBAA0050';
        ctx.textAlign = 'center';
        ctx.font = '11px monospace';
        ctx.fillText(seen.entity.name, seen.entity.location.x, seen.entity.location.y);
    });

    ctx.lineWidth = 4;
    ctx.beginPath();
    ctx.arc(me.location.x, me.location.y, me.radius, 0, Math.PI * 2);
    ctx.stroke();

    ctx.log();
    ctx.log(' * Audition: OK.');
    ctx.log('You hear...');
    
    let audioField = ctx.createField();

    sounds.forEach((sound) => {
        audioField.prelog(`...${sound}`);
    });

    ctx.log();
    ctx.log('----- DONE -----');

    // render logs
    ctx.setTransform(1, 0, 0, 1, 0, 0); // reset transformation to default

    ctx.font = `${fontSize}px monospace`;
    ctx.fillStyle = '#AABBFFFF';
    ctx.textAlign = 'start';
    
    let cy = -fontSize - fontMargin;

    logs.forEach((l) => {
        if (l != null)
            ctx.fillText(l, 6, cy += fontSize + fontMargin);
    });
}

function escapeHTML(html) {
    escape.textContent = html;
    return escape.innerHTML;
}

function sendCommand(cmd) {
    cmd.password = password;
    socket.emit('command', JSON.stringify(cmd));
}

socket.on('sound', (message, level) => {
    let id = flaker();
    sounds.set(id, `${message} (${Math.round(level)} dB)`);

    setTimeout(() => {
        sounds.delete(id);
    }, 12000); // 12 second sounds :D
});

socket.on('deltaSnapshot', (delta) => {
    if (me == null) return;

    if ( SMWorld.applyDelta(delta) ) {
        if (delta.diffs[0] != null)
            updates += delta.diffs[0].changes.length;
        
        if (me != null)
            me = SMWorld.Entity.all.get(me.id);

        updateConsole();
    }

    else
        requestSnapshot();
});

function requestSnapshot() {
    socket.emit('requestSnapshot');
}

socket.on('status update', (sit) => {
    situation = sit;
});

socket.on('updateSnapshot', (save) => {
    if (me == null) return;

    let s = JSON.parse(save);

    SMWorld.loadSave(s);
    fullUpdates++;
    // window.SMW_Save = s;

    if (me != null)
        me = SMWorld.Entity.all.get(me.id);
    
    updateConsole();
});

function addChatMessage(msg) {
    let elmsg = document.createElement('div');
    elmsg.className = 'chat-message';
    elmsg.innerHTML = `<p><b>${escapeHTML(msg.author)}</b></p> ${escapeHTML(msg.content)}`;

    document.querySelector('.chat-area').appendChild(elmsg);

    setTimeout(() => {
        let ar = document.querySelector('.chat-area');
        ar.scrollTop = ar.scrollHeight;
    }, 200);
}

window.authenticate = function authenticate(name, pcode) {
    if (!pcode) pcode = null;

    socket.emit('authenticate', JSON.stringify({
        name: name,
        pcode: pcode
    }));
};

window.sendChat = function sendChat(msg) {
    socket.emit('chat', JSON.stringify({
        password: password,
        message: msg
    }));
};

socket.on('chat', (msg) => {
    if (me == null) return;
    addChatMessage(JSON.parse(msg));
});

socket.on('authStatus', (status) => {
    status = JSON.parse(status);

    if (!status.success) {
        window.alert(`Authentication failed!\n(${status.message})\nCode: ${status.code}`);
    }
    
    else {
        password = status.password;
        SMWorld.loadSave(status.snapshot);
        updates++;

        setInterval(() => {
            $('.update-count').html(`<div>Delta updates per second: ${updates}</div>`);
            $('.full-update-count').html(`<div>Full snapshot updates per second: ${fullUpdates}</div>`);
            updates = 0;
            fullUpdates = 0;
        }, 1000);

        document.querySelector('.chat-container').innerHTML += '<textarea class="chat-input" onkeypress="if (event.keyCode === 13) { sendChat(this.value); setTimeout(() => this.value = \'\', 50); }"></textarea>';
        document.querySelector('.chat-container').innerHTML += '<button onclick="sendChat(this.previousElementSibling.value); setTimeout(() => this.previousElementSibling.value = \'\', 50);">Send</button>';
        document.querySelector('.game-container').innerHTML = `<b>Player Code: ${status.playerCode}</b>${document.querySelector('.game-container').innerHTML}<div id="action" class="flex-column"></div>`;
        document.querySelector('.game-console').innerHTML = '';
        document.querySelector('.game-console').classList.add('monospace');

        commandInput();

        status.players.forEach((p) => {
            let item = document.createElement('div');
            item.className = 'player-item';
            item.setAttribute('pname', escapeHTML(p));
            item.innerHTML = escapeHTML(p);

            document.querySelector('.player-list').appendChild(item);
        });

        me = SMWorld.Entity.all.get(status.you);
        window.SMW_Save = status.snapshot;
        window.you = status.you;

        let cons = document.querySelector('.game-console');
        let jcons = $(cons);

        wd = jcons.width();
        hg = jcons.height();

        updateConsole();
    }
});

socket.on('joined', (player) => {
    if (me == null) return;
    player = JSON.parse(player);

    SMWorld.Entity.fromData(player.entity).postInit();

    let item = document.createElement('div');
    item.className = 'player-item';
    item.setAttribute('pname', escapeHTML(player.name));
    item.innerHTML = escapeHTML(player.name);

    document.querySelector('.player-list').appendChild(item);

    updateConsole();
});

socket.on('left', (name, eid) => {
    if (me == null) return;
    SMWorld.Entity.all.get(eid).destroy();

    $($(`.player-item[pname="${escapeHTML(name)}"]`)[0]).remove();

    updateConsole();
});