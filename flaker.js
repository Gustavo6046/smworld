const FlakeId = require('flake-idgen');
const crypto = require('crypto');

module.exports = function flaker(options) {
    const flakeGen = new FlakeId(options);

    return function() {
        let buf = Buffer.concat([flakeGen.next(), crypto.randomBytes(4)]);
        return buf.toString('hex');
    };
};